/**
*@author Pau Callejas
*/

public class Person {

 public String name;
 public String surname;

 private final String fullName;
 private float position;

/**
*It creates a new person by taking the passed String as argument like name and surname
*
*@param _name person's name
*@param _surname person's name 
*/
 public Person(String _name, String _surname) {
  fullName = name + " " + surname;
  name = _name;
  surname = _surname;
 }

/**
*It move's the person by taking the return value from the move method
*
*@param _distance distance to walk
*/
 public void walk(float _distance) {
  position += move(_distance);
 }

/**
*It return a vale in order to move a person
*
*@param _distance distance to move
*@return distance to move
*/
 protected float move(float _distance) {
  return _distance * 1.0f;
 }

/**
*It writes a messag via command line with the String passed as argument
*
*@param _message message to print
*/
 public void speak(String _message) {
  System.out.println(createMessage(_message));
 }

/**
*It creates a message from the String passed as argument
*
*@param _message message to include
*@return created message
*/
 private String createMessage(String _message) {
  return fullName + " (" + position + "m): " + _message;
 }

}
